function limitFunctionCallCount(cb,n){

    if(typeof cb !=='function' || n<0 || n===undefined){
        throw new Error ("your data is insufficient");
    }

    


    function fun(...args){
       
        // console.log(n);
        if(n>0){
            n--;
            return cb(...args);
        }else{
            return null;
        }

    
    }
    return fun;
} 
module.exports=limitFunctionCallCount;