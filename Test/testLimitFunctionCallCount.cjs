var limitFunctionCallCount = require('/home/mithup/jsClosure/limitFunctionCallCount.cjs');


var prob = limitFunctionCallCount(function fun(val) {
    return val + 5;
}, 5);
console.log(prob(1));
console.log(prob(2));
console.log(prob(3));
console.log(prob(3));
console.log(prob(6));
console.log(prob(5));
console.log(prob(3));
console.log(prob(9));
