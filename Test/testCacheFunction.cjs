const cacheFunction = require('/home/mithup/jsClosure/cacheFunction.cjs');

let prob = cacheFunction(function fun(...args) {
    let value = 0;
    for (let val of args) {
        value += val;
    }
//    console.log("call back function");
    return value;
});
console.log(prob(1,2));
// console.log(prob(2));
console.log(prob(3,4,5,6,7,8,9,3,4,5,6,6,7,3,2,4,6));
// console.log(prob(2));
// console.log(prob(3));
// console.log(prob(5));