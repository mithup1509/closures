function cacheFunction(callb) {

    if (typeof arguments[0] !== 'function' || arguments.length === 0) {
        throw new Error("Enter valid function");
    }

    let obj = {};

    function fun(...args) {
        if (args in obj) {
           
            return obj[args] ;
        }else{
            obj[args] = callb(...args);
            return obj[args];
        }
    }
    return fun;
}

module.exports = cacheFunction;
