function counterFactory() {
  let value = 0;

  function increment() {
    value++;
    return value;
  }

  function decrement() {
    value--;
    return value;
  }

  return { increment, decrement };
}


module.exports = counterFactory;
